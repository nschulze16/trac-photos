# photos-ios

the projects uses the MVVM framework

accounts, photos, and races, all get their own models

photos, and races get their own views (along with some other custom views that are used throughout the project)

the viewcontroller acts as a sort of dispatcher and informs other classes when data has been made available

the project makes use of dependency injection to let the different view controllers access the trac api service

the viewcontroller acts as the delegate to this service, when a delegate method has been called, the viewcontroller gets the new data to the appropriate class